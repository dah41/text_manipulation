import re


def open_file_and_strip_lines(file):
    with open(file) as f:
        all_lines = f.readlines()
        for line in all_lines:
            line.rstrip('\n')


def count_men(file):
    summer = 0
    with open(file) as f:
        all_lines = f.readlines()
        for line in all_lines:
            summer += line.count('men')
    print(summer)


def capitalize_women(file):
    with open(file) as f:
        all_lines = f.readlines()
        for line in all_lines:
            print(type(line))
            line = line.replace('women', 'WOMEN')
            print(line)


def contains_blue_devil(file):
    default = False
    with open(file) as f:
        all_lines = f.readlines()
        for line in all_lines:
            if 'Blue Devil' in line:
                default = True
                break
    print(default)
    return default


def find_non_terminal_said(file):
    with open(file) as f:
        all_lines = f.readlines()
        for line in all_lines:
            my_search = re.finditer('said[^.]', line)
            print(my_search)


if __name__ == "__main__":
    open_file_and_strip_lines('example_text.txt')
    count_men('example_text.txt')
    capitalize_women('example_text.txt')
    output = contains_blue_devil('example_text.txt')
    find_non_terminal_said('example_text.txt')